#include "upgrade.h"

namespace upgrade {
	void upGame(int &gameOp, int &caseOp, float &deltaTime, Vector2 &ballPosition, Vector2 &ballSpeed, float &ballRadius, Rectangle &box1, Rectangle &box2, int &PuntPlayerB, int &PuntPlayerA, bool &pause, bool &Point) {
		deltaTime = GetFrameTime();
		if (gameOp == 3) {
			if (IsKeyPressed(KEY_SPACE)) caseOp = 0;
		}

		if (gameOp == 1) {
			if (!pause) {
				if (!Point) {
					bool Collisionbox1;
					bool Collisionbox2;

					ballPosition.x += ballSpeed.x; //ball movement
					ballPosition.y += ballSpeed.y; //ball movement
					Collisionbox1 = CheckCollisionCircleRec(ballPosition, ballRadius, box1); //detect ball and box1 collision 
					Collisionbox2 = CheckCollisionCircleRec(ballPosition, ballRadius, box2); //detect ball and box2 Collision
					if ((ballPosition.x >= (GetScreenWidth() - ballRadius)) || (ballPosition.x <= ballRadius)) ballSpeed.x *= -1.0f; //wall collision
					if ((ballPosition.y >= (GetScreenHeight() - ballRadius)) || (ballPosition.y <= ballRadius)) ballSpeed.y *= -1.0f; //wall collision
					if (IsKeyDown(KEY_W)) box1.y -= 1000 * deltaTime; //control
					if (IsKeyDown(KEY_S)) box1.y += 1000 * deltaTime; //control
					if ((box2.y + 110) >= ballPosition.y) box2.y -= 1000 * deltaTime; //control
					if ((box2.y + 110) <= ballPosition.y) box2.y += 1000 * deltaTime; //control
					if (Collisionbox1 && ballPosition.y >= ((box1.y - 15) - ballRadius)) {
						ballSpeed.x *= -1.05f;
						ballSpeed.y *= 1.06f;
					}
					if (Collisionbox2 && ballPosition.y >= ((box2.y + 15) - ballRadius)) {
						ballSpeed.x *= -1.05f;
						ballSpeed.y *= 1.06f;
					}

					if (ballPosition.x >= (GetScreenWidth() - ballRadius)) {
						PuntPlayerB++;
						Point = true;
					}
					if (ballPosition.x <= (0 + ballRadius)) {
						PuntPlayerA++;
						Point = true;
					}

					if ((box1.y + 220) >= GetScreenHeight()) box1.y = GetScreenHeight() - 220.0f;
					else if (box1.y <= 0) box1.y = 0;

					if ((box2.y + 220) >= GetScreenHeight()) box2.y = GetScreenHeight() - 220.0f;
					else if (box2.y <= 0) box2.y = 0;

					if (IsKeyPressed(KEY_SPACE)) pause = true;
				}
				else
					if (Point) {
						if (PuntPlayerA == 5) {
							if (IsKeyPressed(KEY_SPACE)) {
								ballPosition = { GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f };
								ballSpeed = { 8.0f, 7.0f };
								box1 = { GetScreenHeight() - 850.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								box2 = { GetScreenHeight() + 280.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								PuntPlayerA = 0;
								PuntPlayerB = 0;
								Point = false;
								caseOp = 0;
							}
						}
						if (PuntPlayerB == 5) {
							if (IsKeyPressed(KEY_SPACE)) {
								ballPosition = { GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f };
								ballSpeed = { 8.0f, 7.0f };
								box1 = { GetScreenHeight() - 850.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								box2 = { GetScreenHeight() + 280.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								PuntPlayerA = 0;
								PuntPlayerB = 0;
								Point = false;
								caseOp = 0;
							}
						}
						if (PuntPlayerB <= 4 && PuntPlayerA <= 4) {
							if (IsKeyPressed(KEY_SPACE)) {
								ballPosition = { GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f };
								ballSpeed = { 8.0f, 7.0f };
								box1 = { GetScreenHeight() - 850.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								box2 = { GetScreenHeight() + 280.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								Point = false;
							}
						}
					}
			}
			else
				if (pause) {
					if (IsKeyPressed(KEY_ENTER)) pause = false;
					if (IsKeyPressed(KEY_BACKSPACE)) {
						ballPosition = { GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f };
						ballSpeed = { 8.0f, 7.0f };
						PuntPlayerA = 0;
						PuntPlayerB = 0;
						box1 = { GetScreenHeight() - 850.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
						box2 = { GetScreenHeight() + 280.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
						caseOp = 0;
						pause = false;
					}
				}
		}
		if (gameOp == 2) {
			if (!pause) {
				if (!Point) {
					bool Collisionbox1;
					bool Collisionbox2;

					ballPosition.x += ballSpeed.x; //ball movement
					ballPosition.y += ballSpeed.y; //ball movement
					Collisionbox1 = CheckCollisionCircleRec(ballPosition, ballRadius, box1); //detect ball and box1 collision 
					Collisionbox2 = CheckCollisionCircleRec(ballPosition, ballRadius, box2); //detect ball and box2 Collision
					if ((ballPosition.x >= (GetScreenWidth() - ballRadius)) || (ballPosition.x <= ballRadius)) ballSpeed.x *= -1.0f; //wall collision
					if ((ballPosition.y >= (GetScreenHeight() - ballRadius)) || (ballPosition.y <= ballRadius)) ballSpeed.y *= -1.0f; //wall collision
					if (IsKeyDown(KEY_W)) box1.y -= 1000 * deltaTime; //control
					if (IsKeyDown(KEY_S)) box1.y += 1000 * deltaTime; //control
					if (IsKeyDown(KEY_UP)) box2.y -= 1000 * deltaTime; //control
					if (IsKeyDown(KEY_DOWN)) box2.y += 1000 * deltaTime; //control
					if (Collisionbox1 && ballPosition.y >= ((box1.y - 15) - ballRadius)) ballSpeed.x *= -1.1f; //Collision reaction
					if (Collisionbox2 && ballPosition.y >= ((box2.y + 15) - ballRadius)) ballSpeed.x *= -1.1f; //Collision reaction

					if (ballPosition.x >= (GetScreenWidth() - ballRadius)) {
						PuntPlayerB++;
						Point = true;
					}
					if (ballPosition.x <= (0 + ballRadius)) {
						PuntPlayerA++;
						Point = true;
					}

					if ((box1.y + 220) >= GetScreenHeight()) box1.y = GetScreenHeight() - 220.0f;
					else if (box1.y <= 0) box1.y = 0;

					if ((box2.y + 220) >= GetScreenHeight()) box2.y = GetScreenHeight() - 220.0f;
					else if (box2.y <= 0) box2.y = 0;

					if (IsKeyPressed(KEY_SPACE)) pause = true;
				}
				else
					if (Point) {
						if (PuntPlayerA == 5) {
							if (IsKeyPressed(KEY_SPACE)) {
								ballPosition = { GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f };
								ballSpeed = { 8.0f, 7.0f };
								box1 = { GetScreenHeight() - 850.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								box2 = { GetScreenHeight() + 280.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								PuntPlayerA = 0;
								PuntPlayerB = 0;
								Point = false;
								caseOp = 0;
							}
						}
						if (PuntPlayerB == 5) {
							if (IsKeyPressed(KEY_SPACE)) {
								ballPosition = { GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f };
								ballSpeed = { 8.0f, 7.0f };
								box1 = { GetScreenHeight() - 850.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								box2 = { GetScreenHeight() + 280.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								PuntPlayerA = 0;
								PuntPlayerB = 0;
								Point = false;
								caseOp = 0;
							}
						}
						if (PuntPlayerB <= 4 && PuntPlayerA <= 4) {
							if (IsKeyPressed(KEY_SPACE)) {
								ballPosition = { GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f };
								ballSpeed = { 8.0f, 7.0f };
								box1 = { GetScreenHeight() - 850.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								box2 = { GetScreenHeight() + 280.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
								Point = false;
							}
						}
					}
			}
			else
				if (pause) {
					if (IsKeyPressed(KEY_ENTER)) pause = false;
					if (IsKeyPressed(KEY_BACKSPACE)) {
						ballPosition = { GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f };
						ballSpeed = { 8.0f, 7.0f };
						PuntPlayerA = 0;
						PuntPlayerB = 0;
						box1 = { GetScreenHeight() - 850.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
						box2 = { GetScreenHeight() + 280.0f, GetScreenWidth() / 2.0f - 250, 20, 220 };
						caseOp = 0;
						pause = false;
					}
				}
		}
	}
}