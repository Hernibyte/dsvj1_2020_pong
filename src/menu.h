#ifndef MENU_H
#define MENU_H

#include"Includes.h"
#include"draw.h"

namespace menu {
	void menu(int &caseOp, int &menuOp, int &gameOp);
}

#endif