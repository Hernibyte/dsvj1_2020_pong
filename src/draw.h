#ifndef DRAW_H
#define DRAW_H

#include "Includes.h"
#include"menu.h"

namespace draw {
	void drawMenu(int &menuOp);
	void drawGame(int &gameOp, float &deltaTime, Vector2 ballPosition, Vector2 ballSpeed, float ballRadius, Rectangle box1, Rectangle box2, int &PuntPlayerB, int &PuntPlayerA, bool &pause, bool &Point);
}

#endif