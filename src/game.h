#ifndef GAME_H
#define GAME_H

#include"Includes.h"
#include"menu.h"
#include"upgrade.h"
namespace game {
	void init();
	void upgrate(Vector2 &ballPosition, Vector2 &ballSpeed, float &ballRadius, Rectangle &box1, Rectangle &box2);
	void draw(Vector2 &ballPosition, Vector2 &ballSpeed, float &ballRadius, Rectangle &box1, Rectangle &box2);
	void start();
}

#endif