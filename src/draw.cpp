#include"draw.h"

namespace draw {
	void drawMenu(int &menuOp) {
		switch (menuOp){
		case 0:
				DrawRectangle(GetScreenHeight() - 560, GetScreenWidth() / 2 - 450, 600, 5, BLACK);
				DrawText("PONG", GetScreenHeight() - 380, GetScreenWidth() / 2 - 550, 75, BLACK);
				DrawText("1 PLAYER", GetScreenHeight() - 425, GetScreenWidth() / 2 - 350, 60, RED);
				DrawText("2 PLAYER", GetScreenHeight() - 425, GetScreenWidth() / 2 - 200, 60, BLACK);
				DrawText("CONTROLS", GetScreenHeight() - 405, GetScreenWidth() / 2 - 50, 45, BLACK);
				DrawText("EXIT", GetScreenHeight() - 340, GetScreenWidth() / 2 + 60, 45, BLACK);
			break;
		case 1:
				DrawRectangle(GetScreenHeight() - 560, GetScreenWidth() / 2 - 450, 600, 5, BLACK);
				DrawText("PONG", GetScreenHeight() - 380, GetScreenWidth() / 2 - 550, 75, BLACK);
				DrawText("1 PLAYER", GetScreenHeight() - 425, GetScreenWidth() / 2 - 350, 60, BLACK);
				DrawText("2 PLAYER", GetScreenHeight() - 425, GetScreenWidth() / 2 - 200, 60, RED);
				DrawText("CONTROLS", GetScreenHeight() - 405, GetScreenWidth() / 2 - 50, 45, BLACK);
				DrawText("EXIT", GetScreenHeight() - 340, GetScreenWidth() / 2 + 60, 45, BLACK);
			break;
		case 2:
				DrawRectangle(GetScreenHeight() - 560, GetScreenWidth() / 2 - 450, 600, 5, BLACK);
				DrawText("PONG", GetScreenHeight() - 380, GetScreenWidth() / 2 - 550, 75, BLACK);
				DrawText("1 PLAYER", GetScreenHeight() - 425, GetScreenWidth() / 2 - 350, 60, BLACK);
				DrawText("2 PLAYER", GetScreenHeight() - 425, GetScreenWidth() / 2 - 200, 60, BLACK);
				DrawText("CONTROLS", GetScreenHeight() - 405, GetScreenWidth() / 2 - 50, 45, RED);
				DrawText("EXIT", GetScreenHeight() - 340, GetScreenWidth() / 2 + 60, 45, BLACK);
			break;
		case 3:
				DrawRectangle(GetScreenHeight() - 560, GetScreenWidth() / 2 - 450, 600, 5, BLACK);
				DrawText("PONG", GetScreenHeight() - 380, GetScreenWidth() / 2 - 550, 75, BLACK);
				DrawText("1 PLAYER", GetScreenHeight() - 425, GetScreenWidth() / 2 - 350, 60, BLACK);
				DrawText("2 PLAYER", GetScreenHeight() - 425, GetScreenWidth() / 2 - 200, 60, BLACK);
				DrawText("CONTROLS", GetScreenHeight() - 405, GetScreenWidth() / 2 - 50, 45, BLACK);
				DrawText("EXIT", GetScreenHeight() - 340, GetScreenWidth() / 2 + 60, 45, RED);
			break;
		default:
				
			break;
		}
	}

	void drawGame(int &gameOp, float &deltaTime, Vector2 ballPosition, Vector2 ballSpeed, float ballRadius, Rectangle box1, Rectangle box2, int &PuntPlayerB, int &PuntPlayerA, bool &pause, bool &Point) {
		if (gameOp == 3) {
			DrawText("CONTROLS", GetScreenHeight() - 405, GetScreenWidth() / 2 - 550, 45, BLACK);
			DrawText("press W buttom to player one up", GetScreenHeight() - 800, GetScreenWidth() / 2 - 450, 25, BLACK);
			DrawText("press S buttom to player one down", GetScreenHeight() - 800, GetScreenWidth() / 2 - 400, 25, BLACK);
			DrawText("press up arrow button to player two up", GetScreenHeight() - 250, GetScreenWidth() / 2 - 450, 25, BLACK);
			DrawText("press down arrow button to player two up", GetScreenHeight() - 250, GetScreenWidth() / 2 - 400, 25, BLACK);
			DrawText("PRESS SPACE BACK TO MENU", GetScreenHeight() / 2 - 400, GetScreenWidth() / 2 + 200, 50, BLACK);
		}
		if (!pause) {
			if (!Point) {
				DrawRectangleRec(box1, MAROON);
				DrawRectangleRec(box2, MAROON);
				DrawCircle(ballPosition.x, ballPosition.y, ballRadius, GOLD);
				DrawText(FormatText("Puntaje: %i", (int)PuntPlayerA), GetScreenHeight() + 60, GetScreenWidth() - 1220, 20, BLACK);
				DrawText(FormatText("Puntaje: %i", (int)PuntPlayerB), GetScreenHeight() - 720, GetScreenWidth() - 1220, 20, BLACK);
			}
			else
				if (Point) {
					if (PuntPlayerA == 5) {
						DrawRectangleRec(box1, MAROON);
						DrawRectangleRec(box2, MAROON);
						DrawCircle(ballPosition.x, ballPosition.y, ballRadius, GOLD);
						DrawText(FormatText("Puntaje: %i", (int)PuntPlayerA), GetScreenHeight() + 60, GetScreenWidth() - 1220, 20, BLACK);
						DrawText(FormatText("Puntaje: %i", (int)PuntPlayerB), GetScreenHeight() - 720, GetScreenWidth() - 1220, 20, BLACK);
						DrawText("PLAYER TWO WON", GetScreenHeight() / 2 - 190, GetScreenWidth() / 2 - 550, 75, BLACK);
						DrawText("PRESS SPACE TO CONTINUE", GetScreenHeight() / 2 - 400, GetScreenWidth() / 2 + 200, 50, BLACK);
					}
					if (PuntPlayerB == 5) {
						DrawRectangleRec(box1, MAROON);
						DrawRectangleRec(box2, MAROON);
						DrawCircle(ballPosition.x, ballPosition.y, ballRadius, GOLD);
						DrawText(FormatText("Puntaje: %i", (int)PuntPlayerA), GetScreenHeight() + 60, GetScreenWidth() - 1220, 20, BLACK);
						DrawText(FormatText("Puntaje: %i", (int)PuntPlayerB), GetScreenHeight() - 720, GetScreenWidth() - 1220, 20, BLACK);
						DrawText("PLAYER ONE WON!", GetScreenHeight() / 2 - 190, GetScreenWidth() / 2 - 550, 75, BLACK);
						DrawText("PRESS SPACE TO CONTINUE", GetScreenHeight() / 2 - 400, GetScreenWidth() / 2 + 200, 50, BLACK);
					}
					if (PuntPlayerB <= 4 && PuntPlayerA <= 4) {
						DrawRectangleRec(box1, MAROON);
						DrawRectangleRec(box2, MAROON);
						DrawCircle(ballPosition.x, ballPosition.y, ballRadius, GOLD);
						DrawText(FormatText("Puntaje: %i", (int)PuntPlayerA), GetScreenHeight() + 60, GetScreenWidth() - 1220, 20, BLACK);
						DrawText(FormatText("Puntaje: %i", (int)PuntPlayerB), GetScreenHeight() - 720, GetScreenWidth() - 1220, 20, BLACK);
						DrawText("WOOOW YOU SCORED!", GetScreenHeight() / 2 - 190, GetScreenWidth() / 2 - 550, 75, BLACK);
						DrawText("PRESS SPACE TO CONTINUE", GetScreenHeight() / 2 - 400, GetScreenWidth() / 2 + 200, 50, BLACK);
					}
				}
		}
		else
			if (pause) {
				DrawText("PAUSE", GetScreenHeight() - 425, GetScreenWidth() - 980, 75, BLACK);
				DrawText("PRESS ENTER TO CONTINUE", GetScreenHeight() / 2 - 400, GetScreenWidth() / 2 + 200, 50, BLACK);
				DrawText("PRESS BACKSPACE BACK TO MENU", GetScreenHeight() / 2 - 400, GetScreenWidth() / 2 + 50, 50, BLACK);
			}
	}
}