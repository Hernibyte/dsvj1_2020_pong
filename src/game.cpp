#include"game.h"

namespace game {
	int screenWidth = 1250;
	int screenHeight = 900;
	int caseOp = 0;
	int menuOp = 0;
	float deltaTime = 0.0f;
	bool pause = false;
	bool Point = false;
	int PuntPlayerB = 0;
	int PuntPlayerA = 0;
	int gameOp = 0;

	struct player {
		Rectangle box;
	};

	struct ball {
		Vector2 ballPosition;
		Vector2 ballSpeed;
		float ballRadius;
	};

	void init() {
		InitWindow(screenWidth, screenHeight, "PONG GAME");

		SetTargetFPS(60);
	}
	void upgrate(Vector2 &ballPosition, Vector2 &ballSpeed, float &ballRadius, Rectangle &box1, Rectangle &box2) {
		//
		switch (caseOp) {
		case 0:
				menu::menu(caseOp, menuOp, gameOp);
			break;
		case 1:
				upgrade::upGame(gameOp, caseOp, deltaTime, ballPosition, ballSpeed, ballRadius, box1, box2, PuntPlayerB, PuntPlayerA, pause, Point);
			break;
		default:
			break;
		}
		//
	}
	void draw(Vector2 &ballPosition, Vector2 &ballSpeed, float &ballRadius, Rectangle &box1, Rectangle &box2) {
		BeginDrawing();
		ClearBackground(RAYWHITE);
		//
		switch (caseOp)
		{
		case 0:
				draw::drawMenu(menuOp);
			break;
		case 1:
				draw::drawGame(gameOp, deltaTime, ballPosition, ballSpeed, ballRadius, box1, box2, PuntPlayerB, PuntPlayerA, pause, Point);
			break;
		default:
			break;
		}
		//
		EndDrawing();
	}
	void start() {
		init();

		player Pj1;
		Pj1.box = { screenHeight - 850.0f, screenWidth / 2.0f - 250, 20, 220 };

		player Pj2;
		Pj2.box = { screenHeight + 280.0f, screenWidth / 2.0f - 250, 20, 220 };

		ball BallF;
		BallF.ballPosition = { screenWidth / 2.0f, screenHeight / 2.0f };
		BallF.ballSpeed = { 8.0f, 7.0f };
		BallF.ballRadius = 20;
		
		while (!WindowShouldClose()) {
			upgrate(BallF.ballPosition, BallF.ballSpeed, BallF.ballRadius, Pj1.box, Pj2.box);
			draw(BallF.ballPosition, BallF.ballSpeed, BallF.ballRadius, Pj1.box, Pj2.box);
		}
		CloseWindow();
	}
}