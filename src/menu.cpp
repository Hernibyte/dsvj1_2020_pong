#include"menu.h"

namespace menu {
	void menu(int &caseOp, int &menuOp, int &gameOp) {

		switch (menuOp) {
		case 0:
				if (IsKeyPressed(KEY_DOWN)) menuOp++;
				if (IsKeyPressed(KEY_ENTER)) {
					caseOp = 1;
					gameOp = 1;
				}
			break;
		case 1:
				if (IsKeyPressed(KEY_UP)) menuOp--;
				if (IsKeyPressed(KEY_DOWN)) menuOp++;
				if (IsKeyPressed(KEY_ENTER)) {
					caseOp = 1;
					gameOp = 2;
				}
			break;
		case 2:
				if (IsKeyPressed(KEY_UP)) menuOp--;
				if (IsKeyPressed(KEY_DOWN)) menuOp++;
				if (IsKeyPressed(KEY_ENTER)) {
					caseOp = 1;
					gameOp = 3;
				}
			break;
		case 3:
				if (IsKeyPressed(KEY_UP)) menuOp--;
				if (IsKeyPressed(KEY_ENTER)) {
					CloseWindow();
				}
			break;
		default:
			break;
		}

	}
}