#ifndef UPGRADE_H
#define UPGRADE_H
#include"Includes.h"

namespace upgrade {
	void upGame(int &gameOp, int &caseOp, float &deltaTime, Vector2 &ballPosition, Vector2 &ballSpeed, float &ballRadius, Rectangle &box1, Rectangle &box2, int &PuntPlayerB, int &PuntPlayerA, bool &pause, bool &Point);
}

#endif